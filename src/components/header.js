import React from "react";
import { Link } from "react-router-dom";

const Header = () => (
  <nav className="navbar navbar-expand-lg navbar-light bg-light">
    <div className="container ">
      <div className="navbar-brand">
        <p className="is-size-3">
          <Link to="/" className="text-dark">
           Bella Digital
          </Link>
        </p>
      </div>
    </div>
  </nav>
);

export default Header;
