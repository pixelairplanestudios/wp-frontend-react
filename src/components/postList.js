import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

import ReactGA from 'react-ga';



class PostList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: []
    };
    this.createMarkup = this.createMarkup.bind();
  }

  componentDidMount() {
    ReactGA.initialize([{ trackingId: process.env.REACT_APP_GA_IDENTIFIER, debug: true, gaOptions: { cookieDomain: 'auto' }}])
    ReactGA.pageview(window.location.pathname + window.location.search);
    axios.get("https://data.belladigital.net/wp-json/wp/v2/posts").then(posts => {
      this.setState({
        posts: posts.data
      });
    });
  }

  createMarkup(html) {
    return { __html: html };
  }

  render() {
    return (
      <div>
        {this.state.posts.map(post => (
          <Link to={`/${post.slug}`} key={post.id}>
            <div className="card" key={post.id}>
              <div className=" card-body">
                <h3>{post.title.rendered}</h3>
                <div
                  dangerouslySetInnerHTML={this.createMarkup(
                    post.excerpt.rendered
                  )}
                />
              </div>
            </div>
          </Link>
        ))}
      </div>
    );
  }
}

export default PostList;
