import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Header from "./components/header";
import PostList from "./components/postList";
import PostView from "./components/postView";

import './App.scss';
require('dotenv').config()

class App extends Component {
  render() {
    return (
        <Router >

          <div>
            <Header />
            <section className="section container content">
              <Route exact path="/" component={PostList} />
              <Route path="/:slug" component={PostView} />
            </section>
          </div>

        </Router>
    );
  }
}


export default App;
